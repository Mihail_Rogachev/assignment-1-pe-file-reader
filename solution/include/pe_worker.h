///@file pe_worker.h
///@brief File containing signatures of functions to work with PE format

#ifndef PE_WORKER
#define PE_WORKER

#include <stdio.h>

#include "pe_file.h"


/// @brief Results of reading a PE file
enum read_status{
    READ_OK = 0,     ///< Successfully read the PE file
    READ_WRONG       ///< Error while reading the PE file
};

///@brief Results of writing a PE file
enum write_status{
    WRITE_OK = 0,     ///< Successfully written the PE file
    WRITE_WRONG       ///< Error while writing the PE file
};

///@brief Function to read a PE file
///@param[in] input File that need to read
///@param[in] peFile Pointer to the save result
///@return Returns READ_OK on success, or READ_WRONG on error
enum read_status readPeFile(FILE* input, PEFile* peFile );

///@brief Function to write a PE file
///@param[in] input File, when we may read section for writing
///@param[in] output File for writing section
///@param[in] sectionHeaders Pointer to the save result
///@return Returns WRITE_OK on success, or WRITE_WRONG on error
enum write_status writeSectionHeader(FILE* input, FILE *output, SectionHeaders *sectionHeaders);

///@brief Function to find and get the section header from a PE file from name
///@param[in] peFile Pointer to the PE file
///@param[in] name Name of the section
///@return Returns the section header
SectionHeaders *getSectionHeader(PEFile *peFile, const char* name);

///@brief Function to free memory allocated for a PE file
///@param[in] peFile Pointer to the PE file
void free_pe(PEFile* peFile);

#endif

