///@file pe_file.h
///@brief File contains structure of PE file

#ifndef PE_READER_H
#define PE_READER_H

#include <stdint.h>

/// Offset of the PE header in the file
#define SIGNATURE_START 0x3c

#ifdef _MSC_VER
    #pragma pack(push ,1)
#endif

/*
* Structure of the PE header in the file
*/
typedef struct
    #if defined __clang__ || defined __GNUC_ 
        __attribute__((packed))
    #endif
 PEHeader{

    ///The number that identifies the type of target machine
    uint16_t Machine;
    ///The number of sections. This indicates the size of the section table, which immediately follows the headers.
    uint16_t NumberOfSections;
    ///The low 32 bits of the number of seconds since 00:00 January 1, 1970 (a C run-time time_t value), which indicates when the file was created.
    uint32_t TimeDateStamp;
    ///The file offset of the COFF symbol table, or zero if no COFF symbol table is present.
    uint32_t PointerToSymbolTable;
    ///The number of entries in the symbol table. This data can be used to locate the string table, which immediately follows the symbol table.
    uint32_t NumberOfSymbols;
    ///The size of the optional header, which is required for executable files but not for object files
    uint16_t SizeOfOptionalHeader;
    ///The flags that indicate the attributes of the file.
    uint16_t Characteristics;

} PEHeader;

/*
* Structure of the optional header in the file
*/
typedef struct
    #if defined __clang__ || defined __GNUC_ 
        __attribute__((packed))
    #endif
 OptionalHeader{
    ///The unsigned integer that identifies the state of the image file.
    uint16_t Magic;
    ///The linker major version number.
    uint8_t MajorLinkerVersion;
    ///The linker minor version number.
    uint8_t MinorLinkerVersion;
    ///The size of the code (text) section, or the sum of all code sections if there are multiple sections.
    uint32_t SizeOfCode;
    ///The size of the initialized data section, or the sum of all such sections if there are multiple data sections.
    uint32_t SizeOfInitializedData;
    ///The size of the uninitialized data section (BSS), or the sum of all such sections if there are multiple BSS sections.
    uint32_t SizeOfUninitializedData;
    ///The address of the entry point relative to the image base when the executable file is loaded into memory.
    uint32_t AddressOfEntryPoint;
    ///The address that is relative to the image base of the beginning-of-code section when it is loaded into memory.
    uint32_t BaseOfCode;

} OptionalHeader;


/*
* Structure of the section headers in the file
*/
typedef struct
    #if defined __clang__ || defined __GNUC_ 
        __attribute__((packed))
    #endif
 SectionHeaders{
    ///An 8-byte, null-padded UTF-8 encoded string. If the string is exactly 8 characters long, there is no terminating null.
    char Name[8];
    ///The total size of the section when loaded into memory. If this value is greater than SizeOfRawData, the section is zero-padded.
    uint32_t VirtualSize;
    ///For executable images, the address of the first byte of the section relative to the image base when the section is loaded into memory.
    uint32_t VirtualAddress;
    ///The size of the section (for object files) or the size of the initialized data on disk (for image files).
    uint32_t SizeOfRawData;
    ///The file pointer to the first page of the section within the COFF file.
    uint32_t PointerToRawData;
    ///The file pointer to the beginning of relocation entries for the section. This is set to zero for executable images or if there are no relocations.
    uint32_t PointerToRelocations;
    ///The file pointer to the beginning of line-number entries for the section. This is set to zero if there are no COFF line numbers.
    uint32_t PointerToLinenumbers;
    ///The number of relocation entries for the section. This is set to zero for executable images.
    uint16_t NumberOfRelocations;
    ///The number of line-number entries for the section.
    uint16_t NumberOfLinenumbers;
    ///The flags that describe the characteristics of the section.
    uint32_t Characteristics;

} SectionHeaders;

/*
* Structure of the PE file in the file
*/
typedef struct
    #if defined __clang__ || __GNUC_ 
        __attribute__((packed))
    #endif
 PEFile{
    ///Offset to the PE header in the file.
    uint32_t MagicSignature;
    ///Indicates the format of the PE file.
    uint32_t MagicNumber;
    /// Main header of the PE file
    PEHeader peHeader;
    ///Optional header of the PE file
    OptionalHeader optionalHeader;
    ///Array of sections in the PE file
    SectionHeaders* sectionHeaders;

} PEFile;

#ifdef _MSC_VER
    #pragma pack(pop)
#endif



#endif
