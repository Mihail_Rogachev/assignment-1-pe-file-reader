///@file pe_worker.c
///@brief Realizer implementation from pe_worker.h
#include <stdlib.h>
#include <string.h>

#include "pe_worker.h"


///@brief Function to read a PE file
///@param[in] input File that need to read
///@param[in] peFile Pointer to the save result
///@return Returns READ_OK on success, or READ_WRONG on error
enum read_status readPeFile(FILE *input, PEFile *peFile){

    if(!input || !peFile) return READ_WRONG;

    if(fseek(input, SIGNATURE_START, SEEK_SET) != 0) return READ_WRONG;

    if(fread(&peFile->MagicSignature, sizeof(uint32_t), 1, input) != 1) return READ_WRONG;

    if(fseek(input, peFile->MagicSignature, SEEK_SET)) return READ_WRONG;

    if(fread(&peFile->MagicNumber, sizeof(uint32_t), 1, input) != 1) return READ_WRONG;

    if(fread(&peFile->peHeader, sizeof(PEHeader), 1, input)!= 1) return READ_WRONG;

    if(fseek(input, peFile->peHeader.SizeOfOptionalHeader, SEEK_CUR) != 0) return READ_WRONG;

    peFile->sectionHeaders = malloc(sizeof(SectionHeaders) * peFile->peHeader.NumberOfSections);

    if(fread(peFile->sectionHeaders, sizeof(SectionHeaders), peFile->peHeader.NumberOfSections, input) != peFile->peHeader.NumberOfSections) {
        free(peFile->sectionHeaders);
        return READ_WRONG;
        };


    return READ_OK;

}

///@brief Function to find and get the section header from a PE file from name
///@param[in] peFile Pointer to the PE file
///@param[in] name Name of the section
///@return Returns the section header
SectionHeaders *getSectionHeader(PEFile *peFile, const char* name){
    for(uint16_t i = 0; i < peFile->peHeader.NumberOfSections; i++){
        if(strcmp(peFile->sectionHeaders[i].Name, name) == 0) return &peFile->sectionHeaders[i];
    }

    return NULL;

}

///@brief Function to free memory allocated for a PE file
///@param[in] peFile Pointer to the PE file
void free_pe(PEFile* peFile){
    free(peFile);
}

///@brief Function to write a PE file
///@param[in] input File, when we may read section for writing
///@param[in] output File for writing section
///@param[in] sectionHeaders Pointer to the save result
///@return Returns WRITE_OK on success, or WRITE_WRONG on error
enum write_status writeSectionHeader(FILE* input, FILE *output, SectionHeaders *sectionHeaders){
    if(!output ||!input ||!sectionHeaders) return WRITE_WRONG;

    if(sectionHeaders->PointerToRawData == 0) return WRITE_WRONG;

    if(fseek(input, sectionHeaders->PointerToRawData, SEEK_SET)!= 0) return WRITE_WRONG;

    char* sectionData = malloc(sectionHeaders->SizeOfRawData);
    
    if(!sectionData){
        free(sectionData);
        return WRITE_WRONG;
    };

    if(fread(sectionData,sectionHeaders->SizeOfRawData,1,input)!= 1){
        free(sectionData);
        return WRITE_WRONG;
        }

    if(fwrite(sectionData, sectionHeaders->SizeOfRawData, 1, output) != 1) {
        free(sectionData);
        return WRITE_WRONG;
    };
    free(sectionData);

    return WRITE_OK;

}



