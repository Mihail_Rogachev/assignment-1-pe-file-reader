/// @file 
/// @brief Main application file

#include <stdio.h>
#include <stdlib.h>


#include "pe_worker.h"



/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f)
{
  fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv)
{
  (void) argc; (void) argv; // suppress 'unused parameters' warning
  if(argc!= 4){
    usage(stdout);
    return 1;
  }

  FILE *in_file = fopen(argv[1], "rb");

  if(!in_file){
    return 1;
  };

  PEFile *pe_file = malloc(sizeof(PEFile));

  if(!pe_file) return 1;

  if(readPeFile(in_file, pe_file)!= READ_OK) {
    free_pe(pe_file);
    fclose(in_file);
    return 1;
  };

  SectionHeaders* section_header = getSectionHeader(pe_file, argv[2]);
  if(!section_header) {
    free(pe_file->sectionHeaders);
    free_pe(pe_file);
    fclose(in_file);
    return 1;
  };

  FILE* out_file = fopen(argv[3], "wb");
  if(!out_file){
    free(pe_file->sectionHeaders);
    free_pe(pe_file);
    fclose(in_file);
    return 1;
  };
  
  if(writeSectionHeader(in_file, out_file, section_header)!= WRITE_OK) {
    free(pe_file->sectionHeaders);
    free_pe(pe_file);
    fclose(in_file);
    fclose(out_file);
    return 1;
  };

  free(pe_file->sectionHeaders);
  free_pe(pe_file);
  fclose(in_file);
  fclose(out_file);
  
  
  return 0;
}
